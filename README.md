# lambda-websocket API Project

This starter project consists of:
* serverless.template - an AWS CloudFormation Serverless Application Model template file for declaring your Serverless functions and other AWS resources
* Functions.cs - class file containing the 3 separate Lambda function handlers for connecting, disconnecting and sending messages.
* aws-lambda-tools-defaults.json - default argument settings for use with Visual Studio and command line deployment tools for AWS

You may also have a test project depending on the options selected.

The generated project contains a Serverless template which declares 3 Lambda functions to connect, disconnect and send messages for the lambda-websocket API. 
The template then declares the WebSocket API with API Gateway and a DynamoDB table to manage connection ids for the open WebSocket connections.

An easy way to test the WebSocket after deploying is to use the tool [wscat](https://github.com/websockets/wscat) from NPM. To install the tool from NPM
use the following command:
```
npm install -g wscat
```

Then to test then open multiple console windows and each console run the following command to connect.
```
wscat -c wss://{YOUR-API-ID}.execute-api.{YOUR-REGION}.amazonaws.com/{STAGE}
```
Note: The url to connect to can also be found as the output parameter of the CloudFormation stack.

In one of the windows use the following command to send a message to the WebSocket which will broadcast the message to all other open console windows:
```
$ wscat -c wss://{YOUR-API-ID}.execute-api.{YOUR-REGION}.amazonaws.com/prod
connected (press CTRL+C to quit)
> {"message":"sendmessage", "data":"hello world"}
< hello world
```

In .NET you can access the WebSocket API using the `System.Net.WebSockets.ClientWebSocket` class. Here is a snippet showing how to send a message to the WebSocketAPI.
```csharp
static async Task Main(string[] args)
{
    var cws = new ClientWebSocket();

    var cancelSource = new CancellationTokenSource();
    var connectionUri = new Uri("wss://{YOUR-API-ID}.execute-api.{YOUR-REGION}.amazonaws.com/prod");
    await cws.ConnectAsync(connectionUri, cancelSource.Token);

    ArraySegment<byte> message = new ArraySegment<byte>(UTF8Encoding.UTF8.GetBytes("{\"message\":\"sendmessage\", \"data\":\"Hello from .NET ClientWebSocket\"}"));
    await cws.SendAsync(message, WebSocketMessageType.Text, true, cancelSource.Token);
}
```

For more information about this demo and and API Gateway WebSocket check out the following blog post which had the original version of 
this demo written in Node.js: https://aws.amazon.com/blogs/compute/announcing-websocket-apis-in-amazon-api-gateway/


## Here are some steps to follow from Visual Studio:

To deploy your Serverless application, right click the project in Solution Explorer and select *Publish to AWS Lambda*.

To view your deployed application open the Stack View window by double-clicking the stack name shown beneath the AWS CloudFormation node in the AWS Explorer tree. The Stack View also displays the root URL to your published application.

## Here are some steps to follow to get started from the command line:

Once you have edited your template and code you can deploy your application using the [Amazon.Lambda.Tools Global Tool](https://github.com/aws/aws-extensions-for-dotnet-cli#aws-lambda-amazonlambdatools) from the command line.

Install Amazon.Lambda.Tools Global Tools if not already installed.
```
    dotnet tool install -g Amazon.Lambda.Tools
```

If already installed check if new version is available.
```
    dotnet tool update -g Amazon.Lambda.Tools
```

Execute unit tests
```
    cd "lambda-websocket/test/Lambda.WebSocket.Tests"
    dotnet test
```

Deploy application
```
    cd "lambda-websocket/src/Lambda.WebSocket"
    dotnet lambda deploy-serverless
```

## Event payload from API Gateway to Lambda

### $connect

When `$connect` with `wscat -c wss://{api_id}.execute-api.eu-west-3.amazonaws.com/Prod` the payload that arrives at the lambda is:
```
{
    "headers": {
        "Host": "4air0zk9w1.execute-api.eu-west-3.amazonaws.com",
        "Sec-WebSocket-Extensions": "permessage-deflate; client_max_window_bits",
        "Sec-WebSocket-Key": "YaGUoXyiYxNlEdxQmooqrg==",
        "Sec-WebSocket-Version": "13",
        "X-Amzn-Trace-Id": "Root=1-6034bfdc-53e605153673d3cb0d0c4177",
        "X-Forwarded-For": "185.153.165.121",
        "X-Forwarded-Port": "443",
        "X-Forwarded-Proto": "https"
    },
    "multiValueHeaders": {
        "Host": [
            "4air0zk9w1.execute-api.eu-west-3.amazonaws.com"
        ],
        "Sec-WebSocket-Extensions": [
            "permessage-deflate; client_max_window_bits"
        ],
        "Sec-WebSocket-Key": [
            "YaGUoXyiYxNlEdxQmooqrg=="
        ],
        "Sec-WebSocket-Version": [
            "13"
        ],
        "X-Amzn-Trace-Id": [
            "Root=1-6034bfdc-53e605153673d3cb0d0c4177"
        ],
        "X-Forwarded-For": [
            "185.153.165.121"
        ],
        "X-Forwarded-Port": [
            "443"
        ],
        "X-Forwarded-Proto": [
            "https"
        ]
    },
    "requestContext": {
        "routeKey": "$connect",
        "disconnectStatusCode": null,
        "messageId": null,
        "eventType": "CONNECT",
        "extendedRequestId": "bMLqcE8YiGYFklQ=",
        "requestTime": "23/Feb/2021:08:42:04 +0000",
        "messageDirection": "IN",
        "disconnectReason": null,
        "stage": "Prod",
        "connectedAt": 1614069724308,
        "requestTimeEpoch": 1614069724309,
        "identity": {
            "cognitoIdentityPoolId": null,
            "cognitoIdentityId": null,
            "principalOrgId": null,
            "cognitoAuthenticationType": null,
            "userArn": null,
            "userAgent": null,
            "accountId": null,
            "caller": null,
            "sourceIp": "185.153.165.121",
            "accessKey": null,
            "cognitoAuthenticationProvider": null,
            "user": null
        },
        "requestId": "bMLqcE8YiGYFklQ=",
        "domainName": "4air0zk9w1.execute-api.eu-west-3.amazonaws.com",
        "connectionId": "bMLqcdPkiGYCGjw=",
        "apiId": "4air0zk9w1"
    },
    "isBase64Encoded": false
}
```

and the APIGatewayProxyRequest is:
```
{
    "Resource": null,
    "Path": null,
    "HttpMethod": null,
    "Headers": {
        "Host": "4air0zk9w1.execute-api.eu-west-3.amazonaws.com",
        "Sec-WebSocket-Extensions": "permessage-deflate; client_max_window_bits",
        "Sec-WebSocket-Key": "YaGUoXyiYxNlEdxQmooqrg==",
        "Sec-WebSocket-Version": "13",
        "X-Amzn-Trace-Id": "Root=1-6034bfdc-53e605153673d3cb0d0c4177",
        "X-Forwarded-For": "185.153.165.121",
        "X-Forwarded-Port": "443",
        "X-Forwarded-Proto": "https"
    },
    "MultiValueHeaders": {
        "Host": [
            "4air0zk9w1.execute-api.eu-west-3.amazonaws.com"
        ],
        "Sec-WebSocket-Extensions": [
            "permessage-deflate; client_max_window_bits"
        ],
        "Sec-WebSocket-Key": [
            "YaGUoXyiYxNlEdxQmooqrg=="
        ],
        "Sec-WebSocket-Version": [
            "13"
        ],
        "X-Amzn-Trace-Id": [
            "Root=1-6034bfdc-53e605153673d3cb0d0c4177"
        ],
        "X-Forwarded-For": [
            "185.153.165.121"
        ],
        "X-Forwarded-Port": [
            "443"
        ],
        "X-Forwarded-Proto": [
            "https"
        ]
    },
    "QueryStringParameters": null,
    "MultiValueQueryStringParameters": null,
    "PathParameters": null,
    "StageVariables": null,
    "RequestContext": {
        "Path": null,
        "AccountId": null,
        "ResourceId": null,
        "Stage": "Prod",
        "RequestId": "bMLqcE8YiGYFklQ=",
        "Identity": {
            "CognitoIdentityPoolId": null,
            "AccountId": null,
            "CognitoIdentityId": null,
            "Caller": null,
            "ApiKey": null,
            "ApiKeyId": null,
            "AccessKey": null,
            "SourceIp": "185.153.165.121",
            "CognitoAuthenticationType": null,
            "CognitoAuthenticationProvider": null,
            "UserArn": null,
            "UserAgent": null,
            "User": null,
            "ClientCert": null
        },
        "ResourcePath": null,
        "HttpMethod": null,
        "ApiId": "4air0zk9w1",
        "ExtendedRequestId": "bMLqcE8YiGYFklQ=",
        "ConnectionId": "bMLqcdPkiGYCGjw=",
        "ConnectionAt": 0,
        "DomainName": "4air0zk9w1.execute-api.eu-west-3.amazonaws.com",
        "DomainPrefix": null,
        "EventType": "CONNECT",
        "MessageId": null,
        "RouteKey": "$connect",
        "Authorizer": null,
        "OperationName": null,
        "Error": null,
        "IntegrationLatency": null,
        "MessageDirection": "IN",
        "RequestTime": "23/Feb/2021:08:42:04 +0000",
        "RequestTimeEpoch": 1614069724309,
        "Status": null
    },
    "Body": null,
    "IsBase64Encoded": false
}
```

and the APIGatewayProxyResponse is:
```
{
    "statusCode": 200,
    "headers": null,
    "multiValueHeaders": null,
    "body": "Connected.",
    "isBase64Encoded": false
}
```

and the payload response is
```
{
    "statusCode": 200,
    "body": "Connected.",
    "isBase64Encoded": false
}
```

### sendmessage
When `sendmessage` `{"message":"sendmessage", "data":"hello world"}` the payload that arrives at the lambda is:
```
{
    "requestContext": {
        "routeKey": "sendmessage",
        "disconnectStatusCode": null,
        "messageId": "bMLu7dQhCGYCGjw=",
        "eventType": "MESSAGE",
        "extendedRequestId": "bMLu7EyrCGYFkkQ=",
        "requestTime": "23/Feb/2021:08:42:33 +0000",
        "messageDirection": "IN",
        "disconnectReason": null,
        "stage": "Prod",
        "connectedAt": 1614069724308,
        "requestTimeEpoch": 1614069753002,
        "identity": {
            "cognitoIdentityPoolId": null,
            "cognitoIdentityId": null,
            "principalOrgId": null,
            "cognitoAuthenticationType": null,
            "userArn": null,
            "userAgent": null,
            "accountId": null,
            "caller": null,
            "sourceIp": "185.153.165.121",
            "accessKey": null,
            "cognitoAuthenticationProvider": null,
            "user": null
        },
        "requestId": "bMLu7EyrCGYFkkQ=",
        "domainName": "4air0zk9w1.execute-api.eu-west-3.amazonaws.com",
        "connectionId": "bMLqcdPkiGYCGjw=",
        "apiId": "4air0zk9w1"
    },
    "body": "{\"message\":\"sendmessage\", \"data\":\"hello world\"}",
    "isBase64Encoded": false
}
```

and the APIGatweayProxyRequest is:
```
{
    "Resource": null,
    "Path": null,
    "HttpMethod": null,
    "Headers": null,
    "MultiValueHeaders": null,
    "QueryStringParameters": null,
    "MultiValueQueryStringParameters": null,
    "PathParameters": null,
    "StageVariables": null,
    "RequestContext": {
        "Path": null,
        "AccountId": null,
        "ResourceId": null,
        "Stage": "Prod",
        "RequestId": "bMLu7EyrCGYFkkQ=",
        "Identity": {
            "CognitoIdentityPoolId": null,
            "AccountId": null,
            "CognitoIdentityId": null,
            "Caller": null,
            "ApiKey": null,
            "ApiKeyId": null,
            "AccessKey": null,
            "SourceIp": "185.153.165.121",
            "CognitoAuthenticationType": null,
            "CognitoAuthenticationProvider": null,
            "UserArn": null,
            "UserAgent": null,
            "User": null,
            "ClientCert": null
        },
        "ResourcePath": null,
        "HttpMethod": null,
        "ApiId": "4air0zk9w1",
        "ExtendedRequestId": "bMLu7EyrCGYFkkQ=",
        "ConnectionId": "bMLqcdPkiGYCGjw=",
        "ConnectionAt": 0,
        "DomainName": "4air0zk9w1.execute-api.eu-west-3.amazonaws.com",
        "DomainPrefix": null,
        "EventType": "MESSAGE",
        "MessageId": "bMLu7dQhCGYCGjw=",
        "RouteKey": "sendmessage",
        "Authorizer": null,
        "OperationName": null,
        "Error": null,
        "IntegrationLatency": null,
        "MessageDirection": "IN",
        "RequestTime": "23/Feb/2021:08:42:33 +0000",
        "RequestTimeEpoch": 1614069753002,
        "Status": null
    },
    "Body": "{\"message\":\"sendmessage\", \"data\":\"hello world\"}",
    "IsBase64Encoded": false
}
```

API Gateway management endpoint: https://4air0zk9w1.execute-api.eu-west-3.amazonaws.com/Prod

and the APIGatewayProxyResponse is:
```
{
    "statusCode": 200,
    "headers": null,
    "multiValueHeaders": null,
    "body": "Data sent to 1 connection",
    "isBase64Encoded": false
}
```

and the response payload is:
```
{
    "statusCode": 200,
    "body": "Data sent to 1 connection",
    "isBase64Encoded": false
}
```

### $disconnect
When `$disconnect` the payload that arrives at the lambda is:
```
{
    "headers": {
        "Host": "4air0zk9w1.execute-api.eu-west-3.amazonaws.com",
        "x-api-key": "",
        "X-Forwarded-For": "",
        "x-restapi": ""
    },
    "multiValueHeaders": {
        "Host": [
            "4air0zk9w1.execute-api.eu-west-3.amazonaws.com"
        ],
        "x-api-key": [
            ""
        ],
        "X-Forwarded-For": [
            ""
        ],
        "x-restapi": [
            ""
        ]
    },
    "requestContext": {
        "routeKey": "$disconnect",
        "disconnectStatusCode": -1,
        "messageId": null,
        "eventType": "DISCONNECT",
        "extendedRequestId": "bMLwLFZdiGYFtLA=",
        "requestTime": "23/Feb/2021:08:42:41 +0000",
        "messageDirection": "IN",
        "disconnectReason": "",
        "stage": "Prod",
        "connectedAt": 1614069724308,
        "requestTimeEpoch": 1614069761070,
        "identity": {
            "cognitoIdentityPoolId": null,
            "cognitoIdentityId": null,
            "principalOrgId": null,
            "cognitoAuthenticationType": null,
            "userArn": null,
            "userAgent": null,
            "accountId": null,
            "caller": null,
            "sourceIp": "185.153.165.121",
            "accessKey": null,
            "cognitoAuthenticationProvider": null,
            "user": null
        },
        "requestId": "bMLwLFZdiGYFtLA=",
        "domainName": "4air0zk9w1.execute-api.eu-west-3.amazonaws.com",
        "connectionId": "bMLqcdPkiGYCGjw=",
        "apiId": "4air0zk9w1"
    },
    "isBase64Encoded": false
}
```

and the APIGatewayRequest is:
```
{
    "Resource": null,
    "Path": null,
    "HttpMethod": null,
    "Headers": {
        "Host": "4air0zk9w1.execute-api.eu-west-3.amazonaws.com",
        "x-api-key": "",
        "X-Forwarded-For": "",
        "x-restapi": ""
    },
    "MultiValueHeaders": {
        "Host": [
            "4air0zk9w1.execute-api.eu-west-3.amazonaws.com"
        ],
        "x-api-key": [
            ""
        ],
        "X-Forwarded-For": [
            ""
        ],
        "x-restapi": [
            ""
        ]
    },
    "QueryStringParameters": null,
    "MultiValueQueryStringParameters": null,
    "PathParameters": null,
    "StageVariables": null,
    "RequestContext": {
        "Path": null,
        "AccountId": null,
        "ResourceId": null,
        "Stage": "Prod",
        "RequestId": "bMLwLFZdiGYFtLA=",
        "Identity": {
            "CognitoIdentityPoolId": null,
            "AccountId": null,
            "CognitoIdentityId": null,
            "Caller": null,
            "ApiKey": null,
            "ApiKeyId": null,
            "AccessKey": null,
            "SourceIp": "185.153.165.121",
            "CognitoAuthenticationType": null,
            "CognitoAuthenticationProvider": null,
            "UserArn": null,
            "UserAgent": null,
            "User": null,
            "ClientCert": null
        },
        "ResourcePath": null,
        "HttpMethod": null,
        "ApiId": "4air0zk9w1",
        "ExtendedRequestId": "bMLwLFZdiGYFtLA=",
        "ConnectionId": "bMLqcdPkiGYCGjw=",
        "ConnectionAt": 0,
        "DomainName": "4air0zk9w1.execute-api.eu-west-3.amazonaws.com",
        "DomainPrefix": null,
        "EventType": "DISCONNECT",
        "MessageId": null,
        "RouteKey": "$disconnect",
        "Authorizer": null,
        "OperationName": null,
        "Error": null,
        "IntegrationLatency": null,
        "MessageDirection": "IN",
        "RequestTime": "23/Feb/2021:08:42:41 +0000",
        "RequestTimeEpoch": 1614069761070,
        "Status": null
    },
    "Body": null,
    "IsBase64Encoded": false
}
```

and the APIGatewayProxyResponse is:
```
{
    "statusCode": 200,
    "headers": null,
    "multiValueHeaders": null,
    "body": "Disconnected.",
    "isBase64Encoded": false
}
```

and the payload response is:
```
{
    "statusCode": 200,
    "body": "Disconnected.",
    "isBase64Encoded": false
}
```

### LOG serialization in CloudWatch
To add marshalling/unmarshalling details, set the following environment variable at the lambda function
`LAMBDA_NET_SERIALIZER_DEBUG`: `true`